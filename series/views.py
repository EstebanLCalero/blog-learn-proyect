from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from series.models import Serie
from series.serializers import SerieSerializer

from rest_framework import viewsets, status
from rest_framework.response import Response
from django.shortcuts import get_object_or_404


# class SeriesAllViewSet(viewsets.ModelViewSet):
    # serializer_class = SerieSerializer


class SeriesViewSet(viewsets.ViewSet):
    model = Serie
    queryset = Serie.objects.all()
    serializer_class = SerieSerializer
    
    def list(self, request):
        serializer = SerieSerializer(self.queryset, many=True)
        return Response(serializer.data)
        pass

    def create(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()          
        return Response(serializer.data, status=status.HTTP_201_CREATED)
        pass

    def retrieve(self, request, pk=None):
        serie = get_object_or_404(self.queryset, pk=pk)
        serializer = SerieSerializer(serie)
        return Response(serializer.data)
        pass

    def update(self, request, pk=None):
        serie = get_object_or_404(self.model, pk=pk)
        serializer = self.serializer_class(serie, data=request.data)
        serializer.is_valid()
        serializer.validated_data
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        serie = get_object_or_404(self.model, pk=pk)
        self.perform_destroy(serie)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

# //////////////////////////////////////////////////////
# //////////////////////////////////////////////////////


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

@csrf_exempt
def serie_list(request):
    if request.method == 'GET':
        series = Serie.objects.all()
        serializer = SerieSerializer(series, many=True)
        return JSONResponse(serializer.data)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = SerieSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def serie_detail(request, pk):
    try:
        serie = Serie.objects.get(pk=pk)
    except Serie.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = SerieSerializer(serie)
        return JSONResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = SerieSerializer(serie, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        serie.delete()
        return HttpResponse(status=204)






