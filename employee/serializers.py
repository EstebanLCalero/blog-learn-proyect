from rest_framework import serilizers

from django.contrib.auth.models import User

class EmployeeSerializer(serilizers.HiperlinkedModelSerializer):
	class Meta:
		model = User
		fields = (
			'first_name',
			'last_name',
			'email',
			'url',
		)